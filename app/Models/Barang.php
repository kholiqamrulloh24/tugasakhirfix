<?php

namespace App\Models;

use Illuminate\Http\Request;
use App\Imports\BarangImport;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Barang extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function import(Request $request)
    {
        $data = $request->file('file');

        $namafile = $data->getClientOriginalName();
        $data->move('DataBarang', $namafile);

        Excel::import(new BarangImport, public_path('/DataBarang/' . $namafile));
    }
    public function delete()
    {
        DB::table('barangs')->truncate();
        DB::table('medoids')->truncate();
        DB::table('distances')->truncate();
    }
}
