<?php

namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class NilaiK extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function tentukan(Request $request)
    {
        DB::table('nilai_k_s')->truncate();
        DB::table('medoids')->truncate();
        DB::table('distances')->truncate();
        NilaiK::create($request->all());
        $inputk = $request->nilaik;
        $random = DB::table('barangs')->inRandomOrder()->take($inputk)->get()->toArray();
        $newmedoids = [];
        foreach ($random as $item) {
            $newmedoid = [];
            $newmedoid['id_barang'] = $item->id_barang;
            $newmedoid['nama_barang'] = $item->nama_barang;
            $newmedoid['total_stock_barang_awal'] = $item->total_stock_barang_awal;
            $newmedoid['transaksi'] = $item->transaksi;
            $newmedoid['barang_terjual'] = $item->barang_terjual;
            $newmedoid['sisa_barang'] = $item->sisa_barang;
            $newmedoid['harga_beli'] = $item->harga_beli;
            $newmedoid['harga_jual'] = $item->harga_jual;
            $newmedoid['keuntungan'] = $item->keuntungan;
            $newmedoid['ratarata_penjualan_pertahun'] = $item->ratarata_penjualan_pertahun;
            $newmedoid['tahun'] = $item->tahun;
            $newmedoid['iterasi'] = 1;
            $newmedoids[] = $newmedoid;
        }
        DB::table('medoids')->insert($newmedoids);
    }
}
