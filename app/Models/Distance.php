<?php

namespace App\Models;

use App\Models\Barang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class Distance extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function barang()
    {
        return $this->belongsTo(Barang::class, 'id_barang');
    }

    public function ambilIterasi()
    {
        $totalIterasi = DB::table('distances')->selectRaw('max(iterasi) as maxIterasi')->get()->first()->maxIterasi;
        $iterasiDiAmbil = $totalIterasi - 1;

        return $iterasiDiAmbil;
    }

    public function ambilIterasiMedoid()
    {
        $totalIterasiMedoid = DB::table('distances')->selectRaw('max(iterasi) as maxIterasi')->get()->first()->maxIterasi;
        $iterasiDiAmbilMedoid = $totalIterasiMedoid - 1;

        return $iterasiDiAmbilMedoid;
    }

    public function ambilIterasiDistances()
    {
        $totalIterasiDistances = DB::table('distances')->selectRaw('max(iterasi) as maxIterasi')->get()->first()->maxIterasi;
        $iterasiDiAmbilDistances = $totalIterasiDistances - 1;

        return $iterasiDiAmbilDistances;
    }

    public function dataBarang()
    {
        $totalIterasiDistances = DB::table('distances')->selectRaw('max(iterasi) as maxIterasi')->get()->first()->maxIterasi;
        $iterasiDiAmbilDistances = $totalIterasiDistances - 1;
        $barang = DB::table('barangs')->join('distances', 'distances.id_barang', '=', 'barangs.id')->where('iterasi', $iterasiDiAmbilDistances)->get()->toArray();
        return $barang;
    }

    public function getTotalIterasi()
    {
        $totalIterasi = DB::table('distances')->selectRaw('max(iterasi) as maxIterasi')->get()->first()->maxIterasi;
        return $totalIterasi;
    }

    public function dataBarangTercluster()
    {
        $iterasiDiAmbilDistances = $this->ambilIterasiDistances();
        $groupCluster = DB::table('distances')->selectRaw('medoid')->groupBy('medoid')->get()->toArray();
        $barangTercluster = [];
        for ($i = 0; $i < count($groupCluster); $i++) {
            $dataBarangCluster = Barang::join('distances', 'barangs.id', '=', 'distances.id_barang')->where('iterasi', $iterasiDiAmbilDistances)->where('medoid', '=', $groupCluster[$i]->medoid)->get()->toArray();
            $barangTercluster[] = $dataBarangCluster;
        }
        return $barangTercluster;
    }

    public function iterasi()
    {
        $totalIterasi = DB::table('distances')->selectRaw('max(iterasi) as maxIterasi')->get()->first()->maxIterasi;
        $iterasiDiAmbil = $totalIterasi;

        return $iterasiDiAmbil;
    }
}
