<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function setStatusTrue()
    {
        $kmedoidStatus = Status::where('proses_id', 'kmedoidsclustering')->get()->first();
        $kmedoidStatus->loading = true;
        $kmedoidStatus->save();
    }

    public function setStatusFalse()
    {
        $kmedoidStatus = Status::where('proses_id', 'kmedoidsclustering')->get()->first();
        $kmedoidStatus->loading = false;
        $kmedoidStatus->save();
    }
}
