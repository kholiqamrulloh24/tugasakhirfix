<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Distance;
use App\Models\Status;

class prosesKmedoid implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        DB::table('distances')->truncate();

        $status = new Status();
        $status->setStatusTrue();

        $databarang = DB::table('barangs')->get()->toArray();
        $jumlahdataset = count($databarang);

        //Perhitungan Euclidean Distance
        $iterasi = 1;
        $lanjutkaniterasi = true;
        while ($lanjutkaniterasi) {
            $datamedoid = DB::table('medoids')->where('iterasi', $iterasi)->get()->toArray();
            $jumlahmedoid = count($datamedoid);
            for ($i = 0; $i < $jumlahdataset; $i++) {
                $jarakterkecil = 100;
                $indexjarakterkecil = 0;
                $barang = $databarang[$i];
                for ($j = 0; $j < $jumlahmedoid; $j++) {
                    $medoid = $datamedoid[$j];
                    $jarak = sqrt(pow($barang->total_stock_barang_awal - $medoid->total_stock_barang_awal, 2) +
                        pow($barang->transaksi - $medoid->transaksi, 2) + pow($barang->barang_terjual - $medoid->barang_terjual, 2) +
                        pow($barang->sisa_barang - $medoid->sisa_barang, 2) + pow($barang->keuntungan - $medoid->keuntungan, 2) +
                        pow($barang->ratarata_penjualan_pertahun - $medoid->ratarata_penjualan_pertahun, 2));
                    if ($jarak < $jarakterkecil) {
                        $jarakterkecil = $jarak;
                        $indexjarakterkecil = $j;
                    }
                }
                $distance = [];
                $distance['id_barang'] = $barang->id;
                $distance['iterasi'] = $iterasi;
                $distance['distance'] = $jarakterkecil;
                $distance['medoid'] = 'C' . ($indexjarakterkecil + 1);
                Distance::create($distance);
            }

            if ($iterasi > 1) {
                $totalJarakIterasiSebelumnya = DB::table('distances')->selectRaw('sum(distance) as totalJarak')->groupBy('iterasi')->having('iterasi', '=', $iterasi - 1)->get()->first()->totalJarak;
                $totalJarakIterasiSaatIni = DB::table('distances')->selectRaw('sum(distance) as totalJarak')->groupBy('iterasi')->having('iterasi', '=', $iterasi)->get()->first()->totalJarak;;
                if ($totalJarakIterasiSaatIni > $totalJarakIterasiSebelumnya) {
                    $lanjutkaniterasi = false;
                }
            }

            $iterasi++;
            $random = DB::table('barangs')->inRandomOrder()->take($jumlahmedoid)->get()->toArray();
            $newmedoids = [];
            foreach ($random as $item) {
                $newmedoid = [];
                $newmedoid['id_barang'] = $item->id_barang;
                $newmedoid['nama_barang'] = $item->nama_barang;
                $newmedoid['total_stock_barang_awal'] = $item->total_stock_barang_awal;
                $newmedoid['transaksi'] = $item->transaksi;
                $newmedoid['barang_terjual'] = $item->barang_terjual;
                $newmedoid['sisa_barang'] = $item->sisa_barang;
                $newmedoid['harga_beli'] = $item->harga_beli;
                $newmedoid['harga_jual'] = $item->harga_jual;
                $newmedoid['keuntungan'] = $item->keuntungan;
                $newmedoid['ratarata_penjualan_pertahun'] = $item->ratarata_penjualan_pertahun;
                $newmedoid['tahun'] = $item->tahun;
                $newmedoid['iterasi'] = $iterasi;
                $newmedoids[] = $newmedoid;
            }
            DB::table('medoids')->insert($newmedoids);
        }
        $status->setStatusFalse();
    }
}
