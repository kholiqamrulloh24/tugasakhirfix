<?php

namespace App\Http\Controllers;

use App\Models\Distance;
use Illuminate\Http\Request;

use App\Http\Controllers\DBIController;
use App\Http\Controllers\SilhouetteController;

class InterpretasiController extends Controller
{
    public function index()
    {
        return view('interpretasi.index', [
            'title' => 'Interpretasi',
            'dataIsiCluster' => $this->dataJumlahClusterKe(), //jumlah data yang ada di cluster i
            'jumlahCluster' => $this->dataCluster(), //cluster ada berapa
            'jarakIterasi' => $this->jarakIterasi(), //jumlahIterasi
            'hasilDBI' => $this->nilaiDBI(),
            'hasilSC' => $this->nilaiSC(),
            'iterasi' => $this->iterasi()
        ]);
    }

    public function dataCluster()
    {
        $dataCluster  = new Distance();
        $barang = count($dataCluster->dataBarangTercluster());

        return $barang;
    }

    public function dataJumlahClusterKe()
    {
        $dataCluster = new Distance();
        $barang = $dataCluster->dataBarangTercluster();
        $jumlah = [];
        for ($i = 0; $i < count($barang); $i++) {
            $jumlah[] = count($barang[$i]);
        }

        return $jumlah;
    }

    public function iterasi()
    {
        $data = new Distance();
        return $data->getTotalIterasi();
    }

    public function jarakIterasi()
    {
        $data = new KmedoidsController();
        $jarak = $data->susunHasilClustering();

        return $jarak;
    }

    public function nilaiDBI()
    {
        $hasilDBI = new DBIController();
        return $hasilDBI->prosesDBI();
    }

    public function nilaiSC()
    {
        $hasilSC = new SilhouetteController();
        return $hasilSC->prosesSC();
    }
}
