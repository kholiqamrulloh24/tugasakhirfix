<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\Distance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Calculation\Statistical\Averages;

class SilhouetteController extends Controller
{
    public function index()
    {
        $totalIterasi = DB::table('distances')->selectRaw('max(iterasi) as maxIterasi')->get()->first()->maxIterasi;
        $iterasiDiAmbil = $totalIterasi - 1;
        $barang = DB::table('barangs')->join('distances', 'distances.id_barang', '=', 'barangs.id')->where('iterasi', $iterasiDiAmbil)->get();

        return view('Silhouette.index', [
            'data' => $barang,
            'hasilSC' => $this->prosesSC(),
            'kategori' => $this->getKategoriSC(),
            'title' => 'Silhouette Coefficient'
        ]);
    }

    public function prosesSC()
    {
        $iterasiDistances = new Distance();
        $iterasiDiAmbilDistances = $iterasiDistances->ambilIterasiDistances();
        $barang = $iterasiDistances->dataBarang();
        $groupCluster = DB::table('distances')->selectRaw('medoid')->groupBy('medoid')->get()->toArray();
        $barangTercluster = [];
        for ($i = 0; $i < count($groupCluster); $i++) {
            $dataBarangCluster = Barang::join('distances', 'barangs.id', '=', 'distances.id_barang')->where('iterasi', $iterasiDiAmbilDistances)->where('medoid', '=', $groupCluster[$i]->medoid)->get()->toArray();
            $barangTercluster[] = $dataBarangCluster;
        }

        //hitung jarak dalam cluster
        $ai = [];
        for ($i = 0; $i < count($barangTercluster); $i++) {
            $aiItem = [];
            for ($j = 0; $j < count($barangTercluster[$i]); $j++) {
                $barang1 = $barangTercluster[$i][$j];
                $totalJarak = 0;
                for ($k = 0; $k < count(($barangTercluster[$i])); $k++) {
                    $barang2 = $barangTercluster[$i][$k];
                    $jarak = sqrt(pow($barang1["total_stock_barang_awal"] - $barang2["total_stock_barang_awal"], 2) +
                        pow($barang1["transaksi"] - $barang2["transaksi"], 2) + pow($barang1["barang_terjual"] - $barang2["barang_terjual"], 2) +
                        pow($barang1["sisa_barang"] - $barang2["sisa_barang"], 2) + pow($barang1["keuntungan"] - $barang2["keuntungan"], 2) +
                        pow($barang1["ratarata_penjualan_pertahun"] - $barang2["ratarata_penjualan_pertahun"], 2));

                    $totalJarak = $totalJarak + $jarak;
                }
                $aiItem[] = $totalJarak / (count($barangTercluster[$i]) - 1);
            }
            $ai[] = $aiItem;
        }

        //hitung jarak luar cluster
        $bi = [];
        for ($clusterKe = 0; $clusterKe < count($barangTercluster); $clusterKe++) {
            for ($barangKe = 0; $barangKe < count($barangTercluster[$clusterKe]); $barangKe++) {
                $barang = $barangTercluster[$clusterKe][$barangKe];
                $jarakTerkecil = null;
                for ($clusterPembandingKe = 0; $clusterPembandingKe < count($barangTercluster); $clusterPembandingKe++) {
                    if ($clusterKe == $clusterPembandingKe) {
                        continue;
                    } else {
                        for ($barangPembandingKe = 0; $barangPembandingKe < count($barangTercluster[$clusterPembandingKe]); $barangPembandingKe++) {
                            $barangPembanding = $barangTercluster[$clusterPembandingKe][$barangPembandingKe];
                            $jarak = sqrt(pow($barang["total_stock_barang_awal"] - $barangPembanding["total_stock_barang_awal"], 2) +
                                pow($barang["transaksi"] - $barangPembanding["transaksi"], 2) + pow($barang["barang_terjual"] - $barangPembanding["barang_terjual"], 2) +
                                pow($barang["sisa_barang"] - $barangPembanding["sisa_barang"], 2) + pow($barang["keuntungan"] - $barangPembanding["keuntungan"], 2) +
                                pow($barang["ratarata_penjualan_pertahun"] - $barangPembanding["ratarata_penjualan_pertahun"], 2));

                            if ($jarakTerkecil == null) {
                                $jarakTerkecil = $jarak;
                            } elseif ($jarak < $jarakTerkecil) {
                                $jarakTerkecil = $jarak;
                            }
                        }
                    }
                }
                $bi[$clusterKe][$barangKe] = $jarakTerkecil;
            }
        }

        //nilai SC
        $si = [];
        for ($cluster = 0; $cluster < count($barangTercluster); $cluster++) {
            $totalSi = 0;
            for ($barang = 0; $barang < count($barangTercluster[$cluster]); $barang++) {
                $hasil = ($bi[$cluster][$barang] - $ai[$cluster][$barang]) / max($ai[$cluster][$barang], $bi[$cluster][$barang]);
                $totalSi = $totalSi + $hasil;
            }
            $si[$cluster] = round(($totalSi / count($barangTercluster[$cluster])), 4);
        }
        return $si;
    }

    public function getKategoriSC()
    {
        $data = $this->prosesSC();
        $hasil = 0;
        for ($i = 0; $i < count($data); $i++) {
            $hasil = $hasil + $data[$i];
        }

        $rerata = $hasil / count($data);

        if ($rerata > 0.7 && $rerata <= 1) {
            $kategori = 'Cluster Struktur Kuat';
        } elseif ($rerata > 0.5 && $rerata <= 0.7) {
            $kategori = 'Cluster Struktur Sedang';
        } elseif ($rerata > 0.25 && $rerata <= 0.5) {
            $kategori = 'Cluster Struktur Rendah';
        } elseif ($rerata <= 0.25) {
            $kategori = 'Cluster Struktur Buruk';
        }

        return $kategori;
    }
}
