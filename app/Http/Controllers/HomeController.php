<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Medoid;

class HomeController extends Controller
{
    public function index()
    {
        return view('home.index', [
            'title' => 'Home'
        ]);
    }
}
