<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use Illuminate\Http\Request;

class ImportController extends Controller
{
    public function index()
    {
        $barang = Barang::all();
        return view('import.index', [
            'barang' => $barang,
            'title' => 'Import Data'
        ]);
    }

    public function import(Request $request)
    {
        $import = new Barang();
        $import->import($request);

        return redirect()->back()->with('success', 'Data Barang Berhasil Diimport!');
    }

    public function delete()
    {
        $delete = new Barang();
        $delete->delete();
        return redirect()->back()->with('warning', 'Data Barang Berhasil Dihapus!');
    }
}
