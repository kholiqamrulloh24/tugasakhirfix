<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\Medoid;
use App\Models\NilaiK;
use App\Models\Status;
use App\Models\Distance;
use App\Jobs\prosesKmedoid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KmedoidsController extends Controller
{
    public function index()
    {
        $barang = Barang::all();
        $medoid = Medoid::where('iterasi', 1)->get();
        return view('kmedoids.index', [
            'data' => $medoid,
            'barang' => $barang,
            'title' => 'K-Medoids Clustering',
            'tampilhasilproses' => false
        ]);
    }

    public function tentukan(Request $request)
    {
        $nilaik = new NilaiK();
        $nilaik->tentukan($request);

        return redirect('/nilaiK')->with('nilaik', 'Nilai K sudah ditentukan, silahkan lanjutkan proses clustering');
    }

    function prosesCluster()
    {
        prosesKmedoid::dispatch();
        return true;
    }

    public function isKmedoidLoading()
    {
        $status = Status::where('proses_id', 'kmedoidsclustering')->get()->first();
        $result = [
            'loading' => $status->loading,
        ];
        return response()->json($result);
    }

    public function susunHasilClustering()
    {
        $getTotal = new Distance();
        $hasil = [];
        $totalIterasi = $getTotal->getTotalIterasi();
        for ($iterasiKe = 1; $iterasiKe <= $totalIterasi; $iterasiKe++) {
            $barang = Distance::with('barang')->where('iterasi', $iterasiKe)->get();
            $medoid = Medoid::where('iterasi', $iterasiKe)->get();
            $totalJarak = DB::table('distances')->selectRaw('sum(distance) as totalJarak')->groupBy('iterasi')->where('iterasi', $iterasiKe)->get()->first()->totalJarak;
            $hasil[] = [
                'daftarMedoid' => $medoid,
                'daftarBarang' => $barang,
                'totalJarak' => $totalJarak
            ];
        }

        return $hasil;
    }

    public function hasilCluster()
    {
        return view('kmedoids.hasilclustering', [
            'data' => $this->susunHasilClustering(),
            'title' => 'Hasil Clustering',
            'tampilhasilproses' => true
        ]);
    }
}
