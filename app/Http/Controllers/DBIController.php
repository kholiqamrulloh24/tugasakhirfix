<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\Distance;
use App\Models\Medoid;
use App\Models\NilaiK;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DBIController extends Controller
{
    public function index()
    {
        $totalIterasi = DB::table('distances')->selectRaw('max(iterasi) as maxIterasi')->get()->first()->maxIterasi;
        $iterasiDiAmbil = $totalIterasi - 1;
        $barang = DB::table('barangs')->join('distances', 'distances.id_barang', '=', 'barangs.id')->where('iterasi', $iterasiDiAmbil)->get();

        return view('DBI.index', [
            'data' => $barang,
            'hasilDBI' => $this->prosesDBI(),
            'tampilHasilProses' => true,
            'title' => 'Davies-Bouldin Index'
        ]);
    }

    public function prosesDBI()
    {
        //hitung SSW

        $ambilData = new Distance();
        $iterasiDiAmbil = $ambilData->ambilIterasi();
        // $barang = DB::table('barangs')->join('distances', 'distances.id_barang', '=', 'barangs.id')->where('iterasi', $iterasiDiAmbil)->get();

        $dataSSW = DB::table('distances')->where('iterasi', $iterasiDiAmbil)->selectRaw('medoid, SUM(distance) as totalJarak, count(medoid) as jumlahDataCluster')->groupBy('medoid')->get()->toArray();
        $SSW = [];
        foreach ($dataSSW as $item) {
            $SSW[] = $item->totalJarak / $item->jumlahDataCluster;
        }

        //hitung SSB
        $iterasiDiAmbilMedoid = $ambilData->ambilIterasiMedoid();
        $medoids = DB::table('medoids')->where('iterasi', $iterasiDiAmbilMedoid)->get()->toArray();

        $SSB = [];
        for ($i = 0; $i < count($medoids); $i++) {
            $proses = [];
            for ($j = 0; $j < count($medoids); $j++) {
                $proses[$j] = sqrt(pow($medoids[$i]->total_stock_barang_awal - $medoids[$j]->total_stock_barang_awal, 2) +
                    pow($medoids[$i]->transaksi - $medoids[$j]->transaksi, 2) + pow($medoids[$i]->barang_terjual - $medoids[$j]->barang_terjual, 2) +
                    pow($medoids[$i]->sisa_barang - $medoids[$j]->sisa_barang, 2) + pow($medoids[$i]->keuntungan - $medoids[$j]->keuntungan, 2) +
                    pow($medoids[$i]->ratarata_penjualan_pertahun - $medoids[$j]->ratarata_penjualan_pertahun, 2));
            }
            $SSB[$i] = $proses;
        }

        //hitung rasio
        $rasios = [];
        $totalMaks = 0;
        for ($i = 0; $i < count($medoids); $i++) {
            $proses = [];
            $maks = 0;
            for ($j = 0; $j < count($medoids); $j++) {
                if (count($medoids) == 2 && $SSB[$i][$j] != 0) {
                    $rasio = ($SSW[$i] + $SSW[$j]) / $SSB[$i][$j];
                    $proses[$j] = $rasio;
                    $totalMaks = $rasio;
                } elseif (count($medoids) > 2 && $SSB[$i][$j] != 0) {
                    $rasio = ($SSW[$i] + $SSW[$j]) / $SSB[$i][$j];
                    $proses[$j] = $rasio;
                    if ($rasio > $maks) {
                        $maks = $rasio;
                    }
                }
            }
            $rasios[$i] = $proses;
            $totalMaks = $totalMaks + $maks;
        }

        //hitung DBI
        $DBI = round(((1 / count($medoids)) * $totalMaks), 4);
        return $DBI;
    }
}
