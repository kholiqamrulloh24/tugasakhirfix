<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DBIController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ImportController;
use App\Http\Controllers\Interpretasi;
use App\Http\Controllers\InterpretasiController;
use App\Http\Controllers\KmedoidsController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\NilaiKController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\SilhouetteController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// halaman login
Route::get('/login', [UserController::class, 'index'])->name('login');
Route::post('/login', [UserController::class, 'authenticate']);
Route::post('/logout', [UserController::class, 'logout']);

//halaman register
Route::get('/register', [RegisterController::class, 'index']);
Route::post('/register', [RegisterController::class, 'store']);

//halaman home
Route::get('/home', [HomeController::class, 'index'])->middleware('auth');
Route::get('/homecoba', [HomeController::class, 'cobaModel'])->middleware('auth');

// halaman import data
Route::get('/importData', [ImportController::class, 'index'])->middleware('auth');
Route::post('/hasilImport', [ImportController::class, 'import'])->middleware('auth');
Route::get('/importData/Delete', [ImportController::class, 'delete'])->middleware('auth');

//halaman nilai K
Route::get('/nilaiK', [NilaiKController::class, 'index'])->middleware('auth');
// Route::post('/nilaiK', [NilaiKController::class, 'tentukan'])->middleware('auth');
Route::post('/nilaiK/reset', [NilaiKController::class, 'reset'])->middleware('auth');

//halaman k-medoids
Route::get('/kmedoidsClustering', [KmedoidsController::class, 'index'])->middleware('auth');
Route::post('/kmedoidsClustering', [KmedoidsController::class, 'prosesCluster'])->middleware('auth');
Route::post('/nilaiK', [KmedoidsController::class, 'tentukan'])->middleware('auth');
Route::get('/total', [KmedoidsController::class, 'susunHasilClustering'])->middleware('auth');
Route::get('/hasilCluster', [KmedoidsController::class, 'hasilCluster'])->middleware('auth');
Route::get('/cekStatusLoadingKmedoid', [KmedoidsController::class, 'isKmedoidLoading'])->middleware('auth');


//halaman DBI
Route::get('/davies-bouldinindex', [DBIController::class, 'index'])->middleware('auth');
Route::post('/davies-bouldinindex', [DBIController::class, 'prosesDBI'])->middleware('auth');

//halaman Silhouette
Route::get('/silhouette', [SilhouetteController::class, 'index'])->middleware('auth');
Route::post('/silhouette', [SilhouetteController::class, 'prosesSC'])->middleware('auth');

//halaman interpretasi
Route::get('/interpretasi', [InterpretasiController::class, 'index'])->middleware('auth');
Route::get('/interpretasi1', [InterpretasiController::class, 'iterasi'])->middleware('auth');
