<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{{$title}}</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/sbadmin2/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="vendor/sbadmin2/css/sb-admin-2.min.css" rel="stylesheet">

    {{-- echart --}}
    <script src="js/echarts.min.js"></script>
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-laugh-wink"></i>
                </div>
                <div class="sidebar-brand-text mx-3">K-Medoids Clustering</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="/home">
                    <i class="fa fa-home"></i>
                    <span>Home</span>
                </a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="/importData">
                    <i class="fas fa-file-import"></i>
                    <span>Kelola Data</span>
                </a>
            </li>

            <!-- Nav Item - Utilities Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="/nilaiK">
                    &nbsp;<i class="fab fa-kickstarter-k"></i>
                    <span>Tentukan Nilai K</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages"
                    aria-expanded="true" aria-controls="collapsePages">
                    <i class="fas fa-fw fa-wrench"></i>
                    <span>Clustering</span>
                </a>
                <div id="collapsePages" class="collapse" aria-labelledby="headingPages"
                    data-parent="#accordionSidebar">
                    <div class="bg-blue collapse-inner rounded">
                        <a class="nav-link collapsed" href="kmedoidsClustering">
                            <span>K-Medoids</span>
                        </a>
                        <a class="nav-link collapsed" href="/hasilCluster">
                            <span>Hasil Cluster</span>
                        </a>
                    </div>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
                    aria-expanded="true" aria-controls="collapseUtilities">
                    <i class="fas fa-fw fa-wrench"></i>
                    <span>Evaluasi</span>
                </a>
                <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-blue collapse-inner rounded">
                        <a class="nav-link collapsed" href="/davies-bouldinindex">
                            <span>Davies-Bouldin Index</span>
                        </a>
                        <a class="nav-link collapsed" href="/silhouette">
                            <span>Silhouette Coefficient</span>
                        </a>
                    </div>
                </div>
            </li>
            <li class="nav-item active">
                <a class="nav-link collapsed" href="/interpretasi">
                    <i class="fas fa-fw fa-wrench"></i>
                    <span>Interpretasi</span>
                </a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>
        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <div class="topbar-divider d-none d-sm-block"></div>

                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{auth()->user()->name}}</span>
                                <img class="img-profile rounded-circle" src="vendor/sbadmin2/img/undraw_profile.svg">
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li>
                                    <form action="/logout" method="post">
                                        @csrf
                                        <button type="submit" class="dropdown-item"><i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i> Logout</button>
                                    </form>
                                </li>
                            </ul>
                        </li>

                    </ul>

                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <div style="display: flex">
                        <div style="flex-grow: 1; display: flex; align-items: center; flex-direction:column">
                            <h4 style="text-align: center; color:black"><b>Iterasi</b></h4>
                            <div id="iterasi" style="width: 400px;height:250px"></div>
                            <div style="color: black"><b>jumlah iterasi = {{ $iterasi }} </b></div>
                        </div>
                        <div style="flex-grow: 1; display: flex; align-items: center; flex-direction:column">
                            <h4 style="text-align: center; color:black"><b>Hasil Cluster</b></h4>
                            <div id="cluster" style="width: 400px;height:250px"></div>
                            <div style="color: black; "><b>
                                @foreach ($dataIsiCluster as $index=>$item)
                                    Cluster {{ $index + 1 }} = {{ $item }} &nbsp;&nbsp;
                                @endforeach
                            </b></div>
                        </div>
                    </div><br><br>
                    <div style="display: flex">
                        <div style="flex-grow: 1; display: flex; align-items: center; flex-direction:column">
                            <h4 style="text-align: center; color:black"><b>Davies-Bouldin Index</b></h4>
                            <div id="DBI" style="width: 400px;height:250px"></div>
                        </div>
                        <div style="flex-grow: 1; display: flex; align-items: center; flex-direction:column">
                            <h4 style="text-align: center; color:black"><b>Silhouette Coefficient</b></h4>
                            <div id="silhouette" style="width: 400px;height:250px"></div>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Your Website 2021</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/sbadmin2/vendor/jquery/jquery.min.js"></script>
    <script src="vendor/sbadmin2/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/sbadmin2/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="vendor/sbadmin2/js/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="vendor/sbadmin2/vendor/chart.js/Chart.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="vendor/sbadmin2/js/demo/chart-area-demo.js"></script>
    <script src="vendor/sbadmin2/js/demo/chart-pie-demo.js"></script>

    //cluster charts

    <script type="text/javascript">
        // Initialize the echarts instance based on the prepared dom
        var myChart = echarts.init(document.getElementById('cluster'));

        // Specify the configuration items and data for the chart
        var option = {
            series: [
                {
                type: 'pie',
                data: [
                    @foreach($dataIsiCluster as $index=>$item)
                    {
                        value: '{{ $item }}',
                    
                        name: 'Cluster {{ $index+1 }}'                
                    },
                    @endforeach
                ]
                }
            ]
        };

        // Display the chart using the configuration items and data just specified.
        myChart.setOption(option);
    </script>

    //iterasi charts
    <script>
        var myChart = echarts.init(document.getElementById('iterasi'));
        var option = {
        xAxis: {
            type: 'category',
            data: [
                @foreach($jarakIterasi as $index=>$item)
                    'Iterasi {{ $index + 1 }}',
                @endforeach
            ],
        },
        yAxis: {
            type: 'value'
        },
        series: [
            {
            type: 'bar',
            data: [
                @foreach($jarakIterasi as $index=>$item)
                {
                    value: '{{ $item['totalJarak'] }}',
                },
                @endforeach
            ],
            }
        ]
        };
        myChart.setOption(option);
    </script>

    //silhouette charts
    

    //DBI charts
    <script type="text/javascript">
        var myChart = echarts.init(document.getElementById('DBI'));
        var option = {
            xAxis: {
                type: 'value',
            },
            yAxis: {
                type: 'value'
            },
            series: [
                {
                data: [8, {{ $hasilDBI }}],
                markPoint: {
                    data : [
                        {type: 'min', name: 'Min'}
                    ]
                },
                type: 'line',
                smooth: true,
                }
            ]
            };
        myChart.setOption(option);
    </script>
    <script type="text/javascript">
        var myChart = echarts.init(document.getElementById('silhouette'));
        var option = {
            xAxis: {
                type: 'value'
            },
            yAxis: {
                type: 'value'
            },
            series: [
                {
                data: [ 
                    @foreach($hasilSC as $item)
                        '{{ $item }}',
                    @endforeach
                ],
                markPoint: {
                    data : [
                        {type: 'min', name: 'Min'},
                        {type: 'max', name: 'Max'}
                    ]
                },
                markLine: {
                    data : [
                        {type: 'average', name: 'Avg'},
                    ]
                },
                
                type: 'line',
                smooth: true,
                }
            ]
            };
        myChart.setOption(option);
    </script>
</body>

</html>